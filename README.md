This code gives the eigenvalues of the appropriate weighted adjacency matrix on the symmetric group Sym(n) acting on the 3-subsets of {1,2,3,...,n}. 
Please see https://arxiv.org/abs/2010.00229v1 for more information on this.

**How to run this code**

In the terminal, run `./three-setwise.sage n key` and replace n with the argument of Sym(n) and key with `true` or `false`. If `key = true`, then the eigenvalues will be printed during the calculations. Otherwise, they are printed at the end.


