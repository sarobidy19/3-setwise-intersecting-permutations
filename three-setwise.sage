#!/usr/local/bin/sage

#To run this code, run the command chmod +x three-setwise.sage in the terminal. Then run the command ./three-setwise.sage n key


import sys

def is_derangement(x,P): #input a permutation x and a set P of all partitions of the integer 3 (consisting of)
    u = Permutation(x).cycle_type()
    for y in P:
        if is_submultiset(y,u) == True:
            return False
    return True

def is_submultiset(A,B): #Checks whether A is a submultiset of B
    B = list(B)
    n = len(A)
    a = True
    i = 0
    while i<n and a == True:
        u = A[i]
        if u in B:
            B.remove(u)
        else:
            a = False
        i += 1
    return a

def VA(m):  #initialize the variables that are used in the weightings on the conjugacy classes of Sym(n)
      t = ''
      for i in range(m):
          if i == m-1:
              t += 'X{0}'.format(i)
          else:
              t += 'X{0},'.format(i)
      return var(t)


# n = argument of Sym(n), k = 3, L = a set of conjugacy classes (as partitions of n), M = a set of partitions of n whose corresponding eigenvalue is -1.
#This function checks whether there is a solution to the system of linear equation of conjugacy classes in L and irreducible characters in M.

def weights(n,k,L,M):
    P = Partitions(k)
    G = SymmetricGroup(n)
    l,m = len(L),len(M)   #L = conjugacy class and M irreducible characters
    #print l,m
    V = VA(m)
    CC_size = []
    char_values = []
    Eq = []
    for F in L:
        for x in SymmetricGroup(n).conjugacy_classes_representatives():
            if x.cycle_type() == F:
                CC_size.append([SymmetricGroup(n).conjugacy_class(x).cardinality(),x])
                break
            else:
                pass
    char = [SymmetricGroupRepresentation(M[i]) for i in range(m)]
    print "--------------------------------------------------------------------------------------------------"
    for chi in char:
        phi = chi#.to_character()
        t = 0
        for i in range(m):
            t += V[i]*phi(CC_size[i][1]).trace()*CC_size[i][0]
            print chi,V[i],phi(CC_size[i][1]).trace(),CC_size[i][1].cycle_type()
        if phi == SymmetricGroupRepresentation([n]):
            Eq.append(t == binomial(n,k) -1)  #The maximum eigenvalue is set to be C(n,k) - 1
        else:
            Eq.append(t == -phi(G[0]).trace())  #The eigenvalue corresponding to phi is set to be equal to -1
        print "---------------------------------------------------------------------------------------------------"
    solutions = solve(Eq,V)
    print "\n","Weight possible?", len(solutions) != 0,"\n"
    if len(solutions) == 0:
        return sys.exit()
##################################################################
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    Q = derangement_conjugacy_classes_representatives
    #print len(Q)
    W = [0]*len(Q)
    for i in range(len(Q)):
        u = Q[i].cycle_type()
        #print u
        for j in range(m):
            if u == Partition(L[j]):
                W[i] = solutions[0][j].rhs()
    print "System of linear equations","\n"
    for x in Eq:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    print "Weights"
    for x in solutions[0]:
        print x
    print "------------------------------------------------------------------------------------------------","\n"
    return [solutions[0][i].rhs() for i in range(len(solutions[0]))],W #W is the appropriate wgenvalues_weightshtings for the conjugacy classes of derangements

# This function takes a weighting W of the conjugacy classes of derangements and produces eigenvalues of the corresponding weighted spanning subgraph.
# The argument W is the weights of the conjugacy classes of derangements
# key is an option to print the eigenvalues during the computation

def eigenvalues_weights(n,k,W,key):
    print "Computing the eigenvalues ...",'\n',"this might take a few minutes",'\n'

    P = Partitions(k)
    eigenvalues = []
    G = SymmetricGroup(n)
    derangement_conjugacy_classes_representatives = []
    CC = G.conjugacy_classes_representatives()
    for x in CC:
        if is_derangement(x,P) == True:
            derangement_conjugacy_classes_representatives.append(x)
    #print derangement_conjugacy_classes_representatives
    Irr = G.irreducible_characters()#
    #Irr = SymmetricGroupRepresentations(n)
    for psi in Irr:
        #phi = psi.to_character()
        s = 0
        L = derangement_conjugacy_classes_representatives
        for i in range(len(L)):
            s += (1/psi.degree()) * ( W[i]*G.conjugacy_class(L[i]).cardinality()*psi(L[i]) )
        eigenvalues.append(s)
        if key == 'true':
            print s
        elif key == 'false':
            pass
        else:
            print "Stoped! Set key = true or key = false"
            return sys.exit()

    print "Eigenvalues","\n"
    if is_even(n) == False and n != 9 and n != 7:
        A,B,C = [eigenvalues[i] for i in [0,1,2]]
        var('r1,r2')
        new_solutions = solve([A>=-1,B>=-1,C>=-1],r1,r2)[0]
        #print new_solutions
        a = new_solutions[2].lhs()
        b = new_solutions[3].rhs()
        A = (a+b)/2
        c = new_solutions[0].lhs().substitute(r2 = A)
        d = new_solutions[1].rhs().substitute(r2 = A)
        B = (c+d)/2
        final_eigenvalues = []
        for x in eigenvalues:
            final_eigenvalues.append(x.substitute(r1 = B,r2 = A))
        print "Eigenvalues for a particular weighting",'\n',final_eigenvalues,'\n'
        print 'The largest eigenvalue is ',max(final_eigenvalues),'\n'
        print 'The least eigenvalue is ',min(final_eigenvalues),'\n'
        print "--------------------------------------------------",'\n','The general eigenvalues are','\n'
    elif n == 7:
        A,B = [eigenvalues[i] for i in [0,1]]
        var('r1')
        new_solutions = solve([A>=-1,B>=-1],r1)[0]
        #print new_solutions
        a = new_solutions[0].lhs()
        b = new_solutions[1].rhs()
        A = (a+b)/2
        final_eigenvalues = []
        for x in eigenvalues:
            final_eigenvalues.append(x.substitute(r1 = A))
        print "Eigenvalues for a particular weighting",'\n',final_eigenvalues,'\n'
        print 'The largest eigenvalue is ',max(final_eigenvalues),'\n'
        print 'The least eigenvalue is ',min(final_eigenvalues),'\n'
        print "--------------------------------------------------",'\n','The general eigenvalues are','\n'
    elif n == 8:
        A,B,C,D = [eigenvalues[i] for i in [0,1,2,3]]
        var('r1,r2')
        new_solutions = solve([A>=-1,B>=-1,C>=-1,D>=-1],r1,r2)[6]
        #print new_solutions
        a = new_solutions[2].lhs()
        b = new_solutions[3].rhs()
        A = (a+b)/2
        c = new_solutions[0].lhs().substitute(r2 = A)
        d = new_solutions[1].rhs().substitute(r2 = A)
        B = (c+d)/2
        final_eigenvalues = []
        for x in eigenvalues:
            final_eigenvalues.append(x.substitute(r1 = B,r2 = A))
        print "Eigenvalues for a particular weighting",'\n',final_eigenvalues,'\n'
        print 'The largest eigenvalue is ',max(final_eigenvalues),'\n'
        print 'The least eigenvalue is ',min(final_eigenvalues),'\n'
        print "--------------------------------------------------",'\n','The general eigenvalues are','\n'
    elif n == 10:
        A,B,C = [eigenvalues[i] for i in [0,1,2]]
        var('r1')
        new_solutions = solve([A>=-1,B>=-1,C>=-1],r1)[0]
        final_eigenvalues = []
        for x in eigenvalues:
            final_eigenvalues.append(x.substitute(r1 = new_solutions[0].rhs()))
        print "Eigenvalues for a particular weighting",'\n',final_eigenvalues,'\n'
        print 'The largest eigenvalue is ',max(final_eigenvalues),'\n'
        print 'The least eigenvalue is ',min(final_eigenvalues),'\n'
        print "--------------------------------------------------",'\n','There is a unique wheighting in this case. We need r1 ={0}  \n\nThe general eigenvalues are'.format(new_solutions[0].rhs()),'\n'
    elif is_even(n) == True and n not in [8,10]:
        A,B,C = [eigenvalues[i] for i in [0,1,2]]
        var('r1,r2')
        new_solutions = solve([A>=-1,B>=-1,C>=-1],r1,r2)[5]
        #print new_solutions
        a = new_solutions[2].lhs()
        b = new_solutions[3].rhs()
        A = (a+b)/2
        c = new_solutions[0].lhs().substitute(r2 = A)
        d = new_solutions[1].rhs().substitute(r2 = A)
        B = (c+d)/2
        final_eigenvalues = []
        for x in eigenvalues:
            final_eigenvalues.append(x.substitute(r1 = B,r2 = A))
        print "Eigenvalues for a particular weighting",'\n',final_eigenvalues,'\n'
        print 'The largest eigenvalue is ',max(final_eigenvalues),'\n'
        print 'The least eigenvalue is ',min(final_eigenvalues),'\n'
        print "--------------------------------------------------",'\n','The general eigenvalues are','\n'
    else:
        A,B,C,D,E = [eigenvalues[i] for i in [0,1,2,3,4]]
        var('r1,r2')
        new_solutions = solve([A>=-1,B>=-1,C>=-1,D >=- 1, E >=-1],r1,r2)[5]
        #print new_solutions
        a = new_solutions[2].lhs()
        b = new_solutions[3].rhs()
        A = (a+b)/2
        c = new_solutions[0].lhs().substitute(r2 = A)
        d = new_solutions[1].rhs().substitute(r2 = A)
        B = (c+d)/2
        final_eigenvalues = []
        for x in eigenvalues:
            final_eigenvalues.append(x.substitute(r1 = B,r2 = A))
        print "Eigenvalues for a particular weighting",'\n',final_eigenvalues,'\n'
        print 'The largest eigenvalue is ',max(final_eigenvalues),'\n'
        print 'The least eigenvalue is ',min(final_eigenvalues),'\n'
        print "--------------------------------------------------",'\n','The general eigenvalues are','\n'
    return eigenvalues

def is_Derangement(x):
     if len(Permutation(x).fixed_points()) == 0:
         return True
     else:
         return False

def der_graph(G):
    CC = G.conjugacy_classes_representatives()
    D = []
    for x in CC:
        if is_Derangement(x) == True:
            D += G.conjugacy_class(x).list()
    return Graph(G.cayley_graph(generators = D))

#######################
n = int(sys.argv[1])
key = sys.argv[2] # true or false
k = 3
G = SymmetricGroup(n)
#######################

if n == 4:
    print "EKR property: equivalent to the natural action."

if n == 5: #,
    Y = graphs.KneserGraph(n,3)
    Y.relabel(range(1,Y.order()+1))
    K = Y.automorphism_group()
    M = K.conjugacy_classes_subgroups()
    print "Searching for Sym(5) of degree 10 ..."
    for x in M:
        if x.is_isomorphic(SymmetricGroup(5)) and x.is_transitive() == True and gap.RankAction(x) == 4: #the group has rank three because of the permutation character.
            H = x
    print "Generating the derangement graph ..."
    X = der_graph(H)
    print "EKR property", X.independent_set(value_only = True) == H.stabilizer(1).order()
    print "Max coclique",X.independent_set(value_only = True)
    print "point-stablizer", H.stabilizer(1).order()

if n == 6:
    print "There are two transitive groups of degree 20 that are isotmorphic to Sym(6) and of rank 4"
    Groups = [TransitiveGroup(20,145),TransitiveGroup(20,149)]
    for x in Groups:
        print x
    print "They both have the EKR property","\n","--------------------------------------------------------------------"
    for x in Groups:
        H = x
        X = der_graph(H)
        print x
        print "EKR property", X.independent_set(value_only = True) == H.stabilizer(1).order()
        print "Max coclique",X.independent_set(value_only = True)
        print "point-stablizer", H.stabilizer(1).order()
        print "--------------------------------------------------------------------------------"

if n == 7:
    W = weights(n,k,[[5,1,1],[7],[6,1],[5,2]],[[n],[n-1,1],[n-2,2],[n-3,3]])
    L =  eigenvalues_weights(n,k,W[1],key)
    print L
    var('r1')
    print "------------------------------------------------------------------",'\n'
    print "EKR property = True when ", solve([-3024*r1 + 34>=-1, 168*r1 - 1>=-1],r1)[0]

if n == 9:
    W = weights(n,k,[[n-2,1,1],[n],[n-1,1],[n-5,4,1],[n-2,2]],[[n],[n-1,1],[n-2,2],[n-3,3],[n-1,1]])
    L =  eigenvalues_weights(n,k,W[1],key)
    print L

if n == 8:
    W = weights(n,k,[[6,2],[4,2,2],[6,1,1],[7,1],[8]],[[n],[n-1,1],[n-2,2],[n-3,3],[n-1,1]])
    L =  eigenvalues_weights(n,k,W[1],key)
    print L

if n == 10:
    W = weights(n,k,[[4,4,2],[4,4,1,1],[6,4],[8,1,1]],[[n],[n-1,1],[n-2,2],[n-3,3]])
    L =  eigenvalues_weights(n,k,W[1],key)
    print L

elif n >= 11:
    if is_even(n) == True:
        W = weights(n,k,[[n-6,2,2,2],[n-5,5],[n-6,5,1],[n-6,4,2],[n-6,4,1,1]],[[n],[n-1,1],[n-3,3],[n-2,2],[n-1,1]]) #works for n >= 12 even
    else:
        W = weights(n,k,[[n-2,1,1],[n],[n-1,1],[n-5,4,1],[n-2,2]],[[n],[n-1,1],[n-2,2],[n-3,3],[n-1,1]]) #works for n >= 11 odd

    L =  eigenvalues_weights(n,k,W[1],key)
    print L
